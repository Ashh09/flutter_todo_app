import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/models/task.dart';
import '/providers/user_provider.dart';
import '/utils/api.dart';
import '/widgets/add_task_dialog.dart';
import '/widgets/task_item_tile.dart';

class TaskListScreen extends StatefulWidget {
    @override
    TaskListScreenState createState() => TaskListScreenState();
}
class TaskListScreenState extends State<TaskListScreen> {
    Future<List<Task>>? _futureTask;

     void getTasks(context) {
        final String? accessToken = context.read<UserProvider>().accessToken;

        setState(() {
            _futureTask = API(accessToken).getTasks().catchError((e) {
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(e.toString())));
            });
        });
    }
    void showAddTaskDialog(BuildContext context){ 
        
        showDialog(
            context: context,
            builder : (BuildContext context) => AddTaskDialog()
        ).then((value){
            getTasks(context);
    
        });
    }
    void showSnackBar(BuildContext context, String message) {
        SnackBar snackBar = new SnackBar(
            content: Text(message), 
            duration: Duration(milliseconds: 2000) 
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
    } 
    Widget showTasks(List? tasks){
        //TaskItemTile is a widget
        var cltTasks = tasks!.map((task) => TaskItemTile(task)).toList();
        return ListView(
            children: cltTasks
        );
    }
    
    @override
    void initState() {
        super.initState();

        WidgetsBinding.instance!.addPostFrameCallback((timestamp) {
            getTasks(context);
        });
    }

    @override
    Widget build(BuildContext context) {
        Widget taskListView = FutureBuilder(
            future: _futureTask,
            builder: (context, snapshot) {
                    if(snapshot.hasData){
                        return showTasks(snapshot.data as List);
                    }
                    return Center(
                    child: CircularProgressIndicator()
                );
            }
        );
        return Scaffold(
            appBar: AppBar(title: Text('Todo List')),
            drawer: Drawer(
                child: ListView(
                    children: [
                        ListTile(
                            title: Text('Logout'),
                            onTap: () async {
                                Provider.of<UserProvider>(context, listen: false).setAccessToken(null);
                                Provider.of<UserProvider>(context, listen: false).setUserId(null);
                                
                                SharedPreferences prefs = await SharedPreferences.getInstance();

                                prefs.remove('accessToken');
                                prefs.remove('userId');

                                Navigator.pushNamedAndRemoveUntil(context, '/', (Route<dynamic> route) => false);
                            }
                        )
                    ]
                )
            ),
            body: Container(
                width: double.infinity,
                padding: EdgeInsets.all(16.0),
                child: taskListView
            ),
            floatingActionButton: FloatingActionButton(
                child: Icon(Icons.add),
                backgroundColor: Color.fromRGBO(255, 212, 71, 1),
                foregroundColor: Colors.black,
                onPressed: (){
                    showAddTaskDialog(context);
                }
            )
        );
    }
}