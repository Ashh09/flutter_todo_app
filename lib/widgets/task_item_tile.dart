import '/models/task.dart';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '/providers/user_provider.dart';

import '/utils/api.dart';


class TaskItemTile extends StatefulWidget {
    final Task _task;
    TaskItemTile(this._task);
    @override
    TaskItemTileState createState() => TaskItemTileState();
}
class TaskItemTileState extends State<TaskItemTile> { 
    late bool _isDone;
    @override
    void initState() {
        super.initState();
        setState(() {
            _isDone = widget._task.isDone == 1;
        });
    }
    void showSnackBar(BuildContext context, String message){
    SnackBar snackBar = SnackBar(
        content: Text(message),
        duration: Duration(milliseconds: 2000)
        );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
    @override
    Widget build(BuildContext context) {
        final String? accessToken = Provider.of<UserProvider>(context, listen: false).accessToken;
        return CheckboxListTile(
            title: Text(widget._task.description),
            value: _isDone,
            controlAffinity: ListTileControlAffinity.leading,
            secondary: IconButton(
                icon: Icon(Icons.info),
                onPressed: () {}
            ),
            onChanged: (bool? value) {
                API(accessToken).toggleTaskStatus(
                    id: widget._task.id, 
                    isDone: (_isDone == true) ? 1 : 0
                ).then ((value){
                    if (value == true) {
                        setState(() => _isDone = !_isDone);
                    }
                }).catchError((error){
                    showSnackBar(context, error.message);
                });
            }
        );
    }
}