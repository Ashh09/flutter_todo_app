import 'package:flutter/material.dart';
import 'package:flutter_todo_app/providers/user_provider.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/assets/env/env_android';
import '/providers/user_provider.dart';
import '/screens/login_screen.dart';
import '/screens/register_screen.dart';
import '/screens/task_list_screen.dart';



Future<void> main() async {
    //Initial checks for user's access token fron SharePreferences
    //Determine initial route of app depending on existence of userr's access token 
    WidgetsFlutterBinding.ensureInitialized();

    SharedPreferences prefs = await SharedPreferences.getInstance();
    API_URL = '10.0.2.2';
    String? accessToken = prefs.getString('accessToken');
    String initialRoute = (accessToken != null) ? '/taskList' : '/';
    runApp(App(accessToken, initialRoute));

    
}
class App extends StatelessWidget {
    final String? _accessToken;
    final String _initialRoute;
    App(this._accessToken, this._initialRoute);
    @override
    Widget build(BuildContext context) {
        return ChangeNotifierProvider(
            create: (BuildContext context) => UserProvider(),
            child: MaterialApp(
                theme: ThemeData(
                    primaryColor: Color.fromRGBO(255, 212, 71, 1),
                    elevatedButtonTheme: ElevatedButtonThemeData(
                        style: ElevatedButton.styleFrom(
                            primary: Color.fromRGBO(255, 212, 71, 1), //background color
                            onPrimary: Colors.black //Text colo
                        )
                    )

                ),
                initialRoute: _initialRoute,
                routes: {
                    '/': (context) => LoginScreen(),
                    '/register': (context) => RegisterScreen(),
                    '/taskList': (context) => TaskListScreen()
                },
            )
        );
    }  
}